# PD cheminformatics pipeline

## patRoon non-target and suspect screening
The patRoon_files folder contains all necessary databases and suspect lists.

The patRoon_codes folder contains the code used to run patRoon: 
  - "launcher.R": to select the mzML files, ionization mode (positive or negative), databases, and suspect lists (only in case of suspect screening) to analyze
  - "process.R": patRoon settings for each step of the data analysis (from the extraction of features to the identification of compounds)

## Chemical curation
The PD_chem_curation folder contains documentation on the creation 
of the suspect lists and database files for screening in patRoon. 
