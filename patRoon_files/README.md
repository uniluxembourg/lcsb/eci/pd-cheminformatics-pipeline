

## patRoon files for non-target and suspect screening
- databases folder contains all necessary databases (csv format). Note that for suspect screening only the "PCL_DB" database will be used.
- suspects folder contains all the suspect lists (csv format)
- LC_MS_files folder is empty due to ethical issues (human data can not be shared). This folder should contain all the mzML files as well as the analysis csv, for each different sample (plasma or feces), chromatographic method (RP or HILIC) and ionization mode (positive or negative). Example for plasma RP in positive mode: "LC_MS_files/Plasma_RP/pos/mzml". 
The analysis csv should contain three main columns indicating the location of the mzML files, name, and group (PD, Ctrl, QC or Blank). 

