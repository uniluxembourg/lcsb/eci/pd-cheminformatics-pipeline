=-:  PubChem Co-Occurrence in Literature Directory  :-=

Revised on March 23, 2020. Please direct all questions to info@ncbi.nlm.nih.gov.

This directory contains the information on entities co-occurrence in literature
(currently, PubMed records) provided by PubChem.

Files:

  full.chemical_chemical.tab.gz
  full.chemical_disease.tab.gz
  full.chemical_geneSymbol.tab.gz
  full.disease_chemical.tab.gz
  full.disease_disease.tab.gz
  full.disease_geneSymbol.tab.gz
  full.geneSymbol_chemical.tab.gz
  full.geneSymbol_disease.tab.gz
  full.geneSymbol_geneSymbol.tab.gz

It also contains auxiliary file

   chemicalNeighboring.cid_parentConnectivityGroupRepCid.tab.gz

that describes groups of compounds that are close neighbors.
  
=================================================================================
  FORMAT of files full.*.tab.gz: tab-delimited
  
  Column 1:  Type of literature record identifier (currently, "PMID")
  Column 2:  Literature record identifier value (currently, the value of PMID)
  Column 3:  Publication date (e.g., 2019-10-15)
  Column 4:  Is a review? (true/false)
  Column 5:  Type of the ID the query has (CID, MeSH, GeneSymbol, EC)
  Column 6:  ID value for the query
  Column 7:  Type of the ID the neighbor has (CID, MeSH, GeneSymbol, EC)
  Column 8:  ID value for the neighbor
  Column 9:  Number of records where both the query and the neighbor are
             annotated as text entities
  Column 10: Cooccurrence score between the query and neighbor entities
             in the database
  Column 11: Relevance score - a context-based score between the query and
             neighbor entities in the literature record
	     
=================================================================================
  FORMAT of the auxiliary file
  chemicalNeighboring.cid_parentConnectivityGroupRepCid.tab.gz

  Column 1:  CID
  Column 2:  Identifier of the group of compounds (close neighbors)

  Each group is formed by CIDS that have the same value in column 2.
  
=================================================================================
Implementation notes
 
  Cooccurrence score is defined as int(100*adjustedArticleCount), with
  adjustedArticleCount calculated using the information gain based scoring
  described in https://pubchemdocs.ncbi.nlm.nih.gov/knowledge-panels.

  For example, if two entities cooccur in 300 records, and the adjusted value
  is 235.23, the cooccurrence score value is 23523.

  Relevance score is calculated for a pair of entities that are annotated in a
  given literature record using heuristics. The score is increased for the
  following factors:

  - Both entities are in the title
  - One entity is in the title
  - Entities appear in close proximity in the record
  - Entities appear multiple times
  - Recent publication

Note that for chemical-chemical neighboring, close neighbors of the query are
excluded.

=================================================================================

:-=  Fair Use Disclaimer  =-:

Databases of molecular data on the NCBI FTP site include such examples as
nucleotide sequences (GenBank), protein sequences, macromolecular structures,
molecular variation, gene expression, and mapping data. They are designed to
provide and encourage access within the scientific community to sources of
current and comprehensive information. Therefore, NCBI itself places no
restrictions on the use or distribution of the data contained therein. However,
some submitters of the original data may claim patent, copyright, or other
intellectual property rights in all or a portion of the data they have submitted.
NCBI is not in a position to assess the validity of such claims and, therefore,
cannot provide comment or unrestricted permission concerning the use, copying,
or distribution of the information contained in the molecular databases.

=================================================================================
